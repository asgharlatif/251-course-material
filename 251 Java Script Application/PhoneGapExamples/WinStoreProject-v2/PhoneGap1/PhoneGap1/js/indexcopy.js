﻿(function () {

    function init() {
        //alert(" The device is ready! ");
        //draw();
        window.requestFileSystem(LocalFileSystem.TEMPORARY, 102400, onSFunc, onErr);

    }

    function draw() {

        d3.select("body").append("svg").attr({ width: "100%", height: "100%" }).append("rect").attr({ width: 100, height: 200, x: 50, y: 50, fill: "blue" });

    }


    function onSFunc(fs) {

        console.log(fs.root.fullPath);
        alert(fs.root.fullPath);

        fs.root.createReader().readEntries(SCB, ECB);

    }

    function SCB(entries) {

        for (i = 0; i < entries.length; i++) {

            if (entries[i].isDirectory) {
                alert("Directory");
            }

            else {
                alert("File");
            }
        }
    }

    function ECB() { }


    function onErr(e) {
        console.log(e);
    }

    window.app = window.app || {};
    app.init = init;

})();

